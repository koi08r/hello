CFLAGS ?= -Wall
LDFLAGS ?=

all: clean compile

compile:
	$(CC) hello.c $(CFLAGS) $(LDFLAGS) -o hello

clean:
	rm -vf hello

install:
	install -D -m 0755 hello $(DESTDIR)/usr/bin/hello

